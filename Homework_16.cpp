﻿#include <iostream>
#include <string>

class Animal
{
public:
    virtual void Voice() {std::cout << "Voice!\n";}
};

class Dog : public Animal
{
public:
    void Voice() override { std::cout << "Woof!\n"; }
};

class Cat : public Animal
{
public:
    void Voice() override { std::cout << "Meow!\n"; }
};


class Mouse : public Animal
{
public:
    void Voice() override { std::cout << "Eek!\n"; }
};

int main()
{
    Mouse Jerry;
    Dog Spike;
    Cat Tom;
    const int amt = 6;
    Animal* flock[amt]{ &Tom, &Jerry, &Spike, new Mouse, new Dog, new Cat };



    for (int i = 0; i < amt; i++)
    {
      flock[i]->Voice();
    }



}